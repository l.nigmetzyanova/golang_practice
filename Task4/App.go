package main

import (
	"bufio"
	"fmt"
	"os"
)

type User struct {
	ID       int
	Username string
	Email    string
	Password string
}

type UserRepository struct {
	Filepath string
}

func NewUserRepository(filepath string) *UserRepository {
	return &UserRepository{
		Filepath: filepath,
	}
}
func (userRepository *UserRepository) Save(user User) {
	file, err := os.OpenFile("file.txt", os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	_, err = file.WriteString("email: " + user.Email)
	_, err = file.WriteString("id: " + string(rune(user.ID)))
	_, err = file.WriteString("username:" + user.Username)
	_, err = file.WriteString("password:" + user.Password)
	if err != nil {
		fmt.Println(err)
		return
	}
}

type UserService struct {
	repository *UserRepository
}

func (userService *UserService) SaveUser(user User) {
	userService.repository.Save(user)
}

type Controller struct {
}

func (controller *Controller) SignUpUser() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Enter name: ")
	scanner.Scan()
	name := scanner.Text()
	fmt.Print("Enter password: ")
	scanner.Scan()
	password := scanner.Text()
	user := User{
		Username: name,
		Password: password,
	}
	userService := UserService{}
	userService.SaveUser(user)
}

func main() {

}
