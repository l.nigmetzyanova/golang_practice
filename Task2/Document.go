package main

import (
	"fmt"
	"time"
)

type Document struct {
	description string
	name        string
	copyCount   int
	beginDate   time.Time
	endDate     time.Time
}

func (document *Document) ChangeDescription(newDescription string) {
	(*document).description = newDescription
}

func (document *Document) ChangeCopyCount(newCopyCount int) {
	if newCopyCount >= 0 {
		(*document).copyCount = newCopyCount
	}
}

func (document *Document) PrintData() {
	fmt.Printf("Description: %v \n", document.description)
	fmt.Printf("Name: %v \n", document.name)
	fmt.Printf("CopyCount: %v \n", document.copyCount)
	fmt.Printf("BeginDate: %v \n", document.beginDate)
	fmt.Printf("EndDate: %v \n", document.endDate)

}

func main() {

	doc := Document{

		description: "some description",
		endDate:     time.Now(),
		beginDate:   time.Now(),
		name:        "name",
		copyCount:   1,
	}

	doc.PrintData()
	doc.ChangeDescription("new description")
	doc.ChangeCopyCount(3)
	doc.PrintData()

}
