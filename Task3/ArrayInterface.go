package main

import "fmt"

type ArrayProcessor interface {
	ProcessToArray(array []int) (processedArray []int)
	ArrayToInt(array []int) (number int)
}

type SumAndZeroProcessor struct {
}

func (sumAndZeroProcessor *SumAndZeroProcessor) ProcessToArray(array []int) (processedArray []int) {
	newArray := []int{}
	for i := 0; i < len(array); i++ {
		if array[i] != 0 {
			newArray = append(newArray, array[i])
		}
	}
	return newArray
}

func (multAndNegativeProcessor *MultAndNegativeProcessor) ProcessToArray(array []int) (processedArray []int) {
	newArray := []int{}
	for i := 0; i < len(array); i++ {
		if array[i] >= 0 {
			newArray = append(newArray, array[i])
		}
	}
	return newArray
}

func (sumAndZeroProcessor *SumAndZeroProcessor) ArrayToInt(array []int) (number int) {
	sum := 0
	for i := 0; i < len(array); i++ {
		sum += array[i]
	}
	return sum
}

func (multAndNegativeProcessor *MultAndNegativeProcessor) ArrayToInt(array []int) (number int) {
	mult := 1
	for i := 0; i < len(array); i++ {
		mult *= array[i]
	}
	return mult
}

type MultAndNegativeProcessor struct {
}

func ProcessArray(array []int, processor ArrayProcessor) (processedArray []int, number int) {
	return processor.ProcessToArray(array), processor.ArrayToInt(array)
}

func main() {

	sumAndZeroProcessor := &SumAndZeroProcessor{}
	multAndNegativeProcessor := &MultAndNegativeProcessor{}
	arr := []int{1, 2, 3, 0, 4, -5, 6, -7}
	fmt.Println(ProcessArray(arr, sumAndZeroProcessor))
	fmt.Println(ProcessArray(arr, multAndNegativeProcessor))

}
